package com.va.agents;

import com.va.Logger;
import com.va.agents.message.*;
import com.va.config.Config;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.va.agents.Contract.MSG_IS_BUSY_REQUEST;

public class StationAgent extends BaseAgent {

    public Config.Station config;


    @Override
    protected void setup() {
        super.setup();
        config = (Config.Station) getArguments()[0];

        Logger.shared.logCreation(this);
        registerAsService("delivery-service");
    }

    // Список сервисов локомотивов, используется для того, чтобы
    // распознать факт обратного получения сообщений от всех локомотивов.
    Config.Wagon wagon;
    List<AID> locomotiveList;
    List<Config.Locomotive> freeLocomotives = new LinkedList<>();

    void configureBehaviour() {
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage msg = receive();
                if (msg == null) {
                    block();
                    return;
                }

                Object content = contentOf(msg);
                if (contentOf(msg) instanceof Config.Wagon) {
                    handleDeliveryRequest(msg, content);
                } else if (content instanceof IsBusyAnswer) {
                    handleIsBusyAnswer(msg, (IsBusyAnswer) content);
                } else if (content instanceof LocomotiveArrival) {
                    handleLocomotiveArrival(msg, ((LocomotiveArrival) content));
                }
            }
        });
    }

    private void handleLocomotiveArrival(ACLMessage msg, LocomotiveArrival content) {
        Logger.shared.log("Локомотив " + content.locomotive.name + " прибыл на эту станцию (" + config.name + ")");
        for (Config.Wagon wagonToLoadFromLocomotive : content.locomotive.wagons) {
            if (wagonToLoadFromLocomotive.destination.equals(config)) {
                Logger.shared.log("Вагон " + wagonToLoadFromLocomotive.name + " был доставлен в пункт назначения - " + config.name);
                // Вырубаем вагон
                AID wagonAid = new AID(wagonToLoadFromLocomotive.name, AID.ISLOCALNAME);
                ACLMessage message = new ACLMessage(ACLMessage.INFORM);
                message.addReceiver(wagonAid);
                setContentOf(message, new ShutUpWagon());
                send(message);
            }
        }


        Config.Wagon wagonToLoad = content.wagon;
        if (wagonToLoad == null) return;
        Logger.shared.log("Грузим вагон " + wagonToLoad.name + " на локомотив " + content.locomotive.name);
        config.wagons.remove(wagonToLoad);

        // Оповещаем локомотив о том, что его нагрузили вагоном и пускаем дальше.
        ACLMessage reply = msg.createReply();

        List<Config.Station> path = costFor(config, wagon.destination).path;
        path.add(0, config);

        setContentOf(reply, new DeliverWagon(wagonToLoad, path));
        send(reply);
    }

    private void handleIsBusyAnswer(ACLMessage msg, IsBusyAnswer content) {
        IsBusyAnswer answer = content;
        locomotiveList.remove(msg.getSender());

        if (answer.isBusy) return;
        Logger.shared.log("Найден свободный локомотив "
                + answer.locomotive.name
                + ", на станции "
                + answer.locomotive.currentStation.name);

        freeLocomotives.add(answer.locomotive);
        if (locomotiveList.isEmpty()) {
            handleReceivedAnswerFromAllLocomotives();
        }
    }

    private void handleReceivedAnswerFromAllLocomotives() {
        ChooseBestResult result = chooseBest(freeLocomotives);
        for (int i = 0; i < result.stations.size(); i++) {
            Config.Station station = result.stations.get(i);
            if (station.equals(config)) {
                for (int j = i + 1; j <= result.stations.size(); ++j) {
                    result.stations.remove(i + 1);
                }
                break;
            }
        }

        Config.Locomotive bestLocomotive = result.locomotive;
        if (bestLocomotive != null) {
            Logger.shared.log("Выбран локомотив " + bestLocomotive.name + ", отправляем его к себе на станцию (" + config.name + ") для установки вагона.");
            ACLMessage message = new ACLMessage(ACLMessage.INFORM);
            setContentOf(message, new MoveToStation(result.stations, wagon));
            AID receiverAid = new AID(bestLocomotive.name, AID.ISLOCALNAME);
            message.addReceiver(receiverAid);
            send(message);
        } else {
            Logger.shared.log("Свободные локомотивы (" + freeLocomotives.size() + "шт.) не могут забрать вагон и доставить его до пункта назначения.");
        }
    }

    private void handleDeliveryRequest(ACLMessage message, Object content) {
        wagon = (Config.Wagon) content;
        Boolean hasThisWagon = config.wagons.contains(wagon);

        if (!hasThisWagon) return;

        Logger.shared.log("Станция " + config.name + " ищет локомотив для доставки вагона с названием " + wagon.name);
        freeLocomotives.clear();

        AID[] locomotives = getServiceAids("locomotive-service");
        this.locomotiveList = new LinkedList<>(Arrays.asList(locomotives));

        ACLMessage request = new ACLMessage(ACLMessage.INFORM);
        request.setContent(MSG_IS_BUSY_REQUEST);
        for (AID locomotive : locomotives) {
            request.addReceiver(locomotive);
        }
        send(request);

        // Получить надо тут объект вагона, если его нет в поле wagons
        // станции, то ретурн делаем, а если же есть, то запрашиваем
        // у всех локомотивов, заняты ли они
        // выбираем ближайший и отправляем его к себе
        // когда он сюда приедет, грузим в него вагон, из своего списка дропаем
        // и пускаем локомотив к следующей станции.
        // при этом делаем проверку, есть ли у локомотива вагоны,
        // которым надо на эту станцию

        // сейчас проблема стоит в том, чтобы получить WagonAgent из переданного ACLMessage message
        // хуй знает как это сделать
        // UPD: сделано через content object
    }

    @Override
    protected void takeDown() {
        try {
            DFService.deregister(this);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
        super.takeDown();
    }

    class ChooseBestResult {
        Config.Locomotive locomotive;
        List<Config.Station> stations;

        ChooseBestResult(Config.Locomotive locomotive, List<Config.Station> stations) {
            this.locomotive = locomotive;
            this.stations = stations;
        }
    }
    private ChooseBestResult chooseBest(List<Config.Locomotive> list) {
        Config.Locomotive bestLocomotive = null;
        Long minCost = Long.MAX_VALUE;
        CostResult bestCost = null;

        for (Config.Locomotive locomotive : list) {
            CostResult result = costFor(locomotive.currentStation, wagon.destination);
            if (result == null) continue;
            result.path.add(0, locomotive.currentStation);
            if (minCost > result.cost && result.containsDestination) {
                minCost = result.cost;
                bestLocomotive = locomotive;
                bestCost = result;
            }
        }

        if (minCost < 0) {
            return null;
        }

        System.out.println(bestCost);
        System.out.println(bestCost.cost);
        return new ChooseBestResult(bestLocomotive, bestCost.path);
    }
    class CostResult {
        boolean containsDestination = false;
        long cost = 0;
        LinkedList<Config.Station> path = new LinkedList<>();

        CostResult(boolean containsDestination, long cost) {
            this.containsDestination = containsDestination;
            this.cost = cost;
        }

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder();
            for (Config.Station station : path) {
                result.append(" -> ").append(station.name);
            }
            return result.toString();
        }
    }
    private CostResult costFor(Config.Station from, Config.Station to) {
        if (from.equals(to)) {
            CostResult result = new CostResult(from.equals(config) || to.equals(config), 0);
            result.path.clear();
            return result;
        }

        Long min = Long.MAX_VALUE;
        CostResult result = null;

        for (Config.Station.Road road : from.roads) {
            CostResult costResult = costFor(road.destination, to);
            costResult.path.add(0, road.destination);

            costResult.containsDestination |= from.equals(config) || to.equals(config);
            if (min > costResult.cost + road.timeCost) {
                min = costResult.cost + road.timeCost;
                result = costResult;
                result.cost += road.timeCost;
            }
        }

        return result;
    }

}
