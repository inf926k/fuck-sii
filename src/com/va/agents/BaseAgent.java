package com.va.agents;

import com.sun.istack.internal.Nullable;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import jade.util.leap.Properties;

import java.io.IOException;

abstract class BaseAgent extends Agent {

    @Override
    protected void setup() {
        super.setup();
        configureBehaviour();
    }

    AID[] getServiceAids(String type) {
        DFAgentDescription template = new DFAgentDescription();
        ServiceDescription sd = new ServiceDescription();
        sd.setType(type);
        template.addServices(sd);
        try {
            DFAgentDescription[] result = DFService.search(this, template);
            AID[] agents = new AID[result.length];
            for (int i = 0; i < result.length; ++i) {
                agents[i] = result[i].getName();
            }

            return agents;
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

        throw new RuntimeException("Не удалось получить список сервисов.");
    }


    void registerAsService(String type) {
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(type);
        sd.setName(getClass().getName() + " service");
        dfd.addServices(sd);

        try {
            DFService.register(this, dfd);
        } catch (FIPAException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    Object contentOf(ACLMessage message) {
        try {
            return message.getContentObject();
        } catch (UnreadableException e) {
            return null;
        }
    }

    void setContentOf(ACLMessage message, java.io.Serializable serializable) {
        try {
            message.setContentObject(serializable);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void configureBehaviour() {

    }

    void customSend(ACLMessage message) {
        Properties props = message.getAllUserDefinedParameters();

    }



}
