package com.va.agents;

import com.sun.istack.internal.Nullable;
import com.va.Logger;
import com.va.agents.message.DeliverWagon;
import com.va.agents.message.IsBusyAnswer;
import com.va.agents.message.LocomotiveArrival;
import com.va.agents.message.MoveToStation;
import com.va.config.Config;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.List;

import static com.va.agents.Contract.MSG_IS_BUSY_REQUEST;

public class LocomotiveAgent extends BaseAgent {

    public Config.Locomotive config;

    @Nullable
    private Config.Wagon wagon;

    @Override
    protected void setup() {
        super.setup();
        config = (Config.Locomotive) getArguments()[0];

        Logger.shared.logCreation(this);
        registerAsService("locomotive-service");
    }

    @Override
    void configureBehaviour() {
        super.configureBehaviour();
        addBehaviour(new CyclicBehaviour() {
            @Override
            public void action() {
                ACLMessage msg = receive();
                if (msg == null) {
                    block();
                    return;
                }

                if (msg.getContent().equals(MSG_IS_BUSY_REQUEST)) {
                    handleIsBusyRequest(msg);
                } else if (contentOf(msg) instanceof MoveToStation) {
                    handleMoveToStationCommand(msg);
                } else if (contentOf(msg) instanceof DeliverWagon) {
                    handleDeliverWagonRequest(msg, ((DeliverWagon) contentOf(msg)));
                }
            }
        });
    }

    private void handleDeliverWagonRequest(ACLMessage msg, DeliverWagon deliverWagon) {
        wagon = null;
        Logger.shared.log("На локомотив " + config.name + " был загружен вагон " + deliverWagon.wagon.name);
        Logger.shared.log("Отправляемся по пути " + Config.pathStringOf(deliverWagon.path));
        config.destination = deliverWagon.path.get(deliverWagon.path.size() - 1);
        config.wagons.add(deliverWagon.wagon);
        goPath(deliverWagon.path);
    }

    private void handleMoveToStationCommand(ACLMessage msg) {
        MoveToStation moveToMessage = (MoveToStation) contentOf(msg);
        Logger.shared.log("Получен приказ на движение к станции " + moveToMessage.destination.name + ", начинаю движение");
        Logger.shared.log("Путь " + moveToMessage);
        config.destination = moveToMessage.destination;
        wagon = moveToMessage.wagon;
        goPath(moveToMessage.path);
    }

    private void goPath(List<Config.Station> path) {
        if (path.size() == 1) {
            Config.Station station = path.get(0);
            config.destination = station;
            config.currentStation = station;
            Logger.shared.log("Локомотив " + config.name + " добрался до КОНЕЧНОЙ станции " + station.name);
            notifyStationAboutArrival();

            return;
        }

        Config.Station from = path.get(0);
        Config.Station to = path.get(1);
        Config.Station.Road road = findRoad(from, to);
        Long timeCost = road.timeCost;

        try {
            Thread.sleep(timeCost);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Logger.shared.log("Локомотив " + config.name + " добрался до станции " + to.name + " за время " + road.timeCost);
        path.remove(0);
        goPath(path);
    }

    private void notifyStationAboutArrival() {
        Logger.shared.log("Локомотив оповещает станцию " + config.currentStation.name + " о своем прибытии.");
        AID aid = new AID(config.currentStation.name, AID.ISLOCALNAME);
        ACLMessage message = new ACLMessage(ACLMessage.INFORM);
        message.addReceiver(aid);
        setContentOf(message, new LocomotiveArrival(config, wagon));
        send(message);
    }

    private Config.Station.Road findRoad(Config.Station from, Config.Station to) {
        for (Config.Station.Road road : from.roads) {
            if (road.destination.equals(to)) {
                return road;
            }
        }

        throw new RuntimeException("Путь был построен неверно, отсутствует дорога к соседней станции.");
    }

    private void handleIsBusyRequest(ACLMessage msg) {
        ACLMessage answer = msg.createReply();
        setContentOf(answer, new IsBusyAnswer(config.destination != null, config));
        send(answer);
    }
}
