package com.va.agents.message;

import com.va.config.Config;

import java.io.Serializable;

// Запрос на доставку, отправляется вагонами для станций.
public class IsBusyAnswer implements Serializable {

    public boolean isBusy;

    public Config.Locomotive locomotive;

    public IsBusyAnswer(boolean answer, Config.Locomotive locomotive) {
        this.isBusy = answer;
        this.locomotive = locomotive;
    }

}
