package com.va.agents.message;

import com.va.config.Config;

import java.io.Serializable;

public class LocomotiveArrival implements Serializable {

    public Config.Locomotive locomotive;
    public Config.Wagon wagon;

    public LocomotiveArrival(Config.Locomotive locomotive, Config.Wagon wagon) {
        this.locomotive = locomotive;
        this.wagon = wagon;
    }
}
