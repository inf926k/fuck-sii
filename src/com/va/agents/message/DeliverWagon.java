package com.va.agents.message;

import com.va.config.Config;

import java.io.Serializable;
import java.util.List;

// Сообщение-приказ для вагона, чтобы он доставил вагон на заданную станцию.
public class DeliverWagon implements Serializable {

    public Config.Wagon wagon;
    public List<Config.Station> path;

    public DeliverWagon(Config.Wagon wagon, List<Config.Station> path) {
        this.wagon = wagon;
        this.path = path;
    }
}
