package com.va.agents.message;

import com.va.config.Config;

import java.io.Serializable;
import java.util.List;

// Сообщение локомотиву о том, что надо забрать вагон на некоторой станции.
public class MoveToStation implements Serializable {
    public Config.Station destination;
    public List<Config.Station> path;
    public Config.Wagon wagon;

    public MoveToStation(List<Config.Station> path, Config.Wagon wagon) {
        this.path = path;
        this.destination = path.get(path.size() - 1);
        this.wagon = wagon;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Config.Station station : path) {
            result.append(" -> ").append(station.name);
        }
        return result.toString();
    }
}
