package com.va.agents;

import com.va.Logger;
import com.va.agents.message.ShutUpWagon;
import com.va.config.Config;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;

public class WagonAgent extends BaseAgent {

    public Config.Wagon config;
    private boolean isTransporting = false;

    private boolean block = false;

    class WagonBehaviour extends TickerBehaviour {
        WagonBehaviour(Agent a, long period) {
            super(a, period);
        }

        @Override
        protected void onTick() {
            ACLMessage message = receive();
            handleMsg(message);

            if (!isTransporting)
                requestDelivery();
        }

        private void handleMsg(ACLMessage message) {
            if (message == null) {
                block();
                return;
            }

            String MSG_STOP_TRANSPORTING = "MSG_STOP_TRANSPORTING";
            String MSG_START_TRANSPORTING = "MSG_START_TRANSPORTING";
            if (contentOf(message) instanceof ShutUpWagon) shutUpPlease();
            else if (message.getContent().equals(MSG_START_TRANSPORTING)) isTransporting = true;
            else if(message.getContent().equals(MSG_STOP_TRANSPORTING)) isTransporting = false;
        }

        private void requestDelivery() {
            if (block) return;

            AID[] stationAgents = getServiceAids("delivery-service");
            Logger.shared.log("Вагон " + config.name + " запрашивает доставку у " + stationAgents.length + " станций.");

            ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
            for (AID stationAgent : stationAgents) {
                msg.addReceiver(stationAgent);
            }
//            msg.setContent(MSG_REQUEST_DELIVERY);
            setContentOf(msg, config);

            send(msg);
        }
    }

    private WagonBehaviour behaviour = new WagonBehaviour(this, 1000);

    @Override
    protected void setup() {
        super.setup();
        config = (Config.Wagon) getArguments()[0];

        Logger.shared.logCreation(this);

        addBehaviour(behaviour);
    }

    private void shutUpPlease() {
        removeBehaviour(behaviour);
        Logger.shared.log("Вагон " + config.name + " БЫЛ ДОСТАВЛЕН");
        block = true;
    }
}