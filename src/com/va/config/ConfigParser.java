package com.va.config;

import java.io.*;
import java.util.*;

public class ConfigParser {

    @SuppressWarnings("FieldCanBeLocal")
    private static String FILE_NAME = "config.txt";

    public Config parse() {
        String content = readFileContents();
        if (content == null) {
            System.out.println("Не удалось прочитать содержимое файла конфигураций.");
            return null;
        }

        ArrayList<String> lines = new ArrayList<>(Arrays.asList(content.split("\n")));

        List<Config.Station> stationList = readStations(lines);
        List<Config.Wagon> wagonList = readWagons(lines, stationList);
        List<Config.Locomotive> locomotiveList = readLocomotives(lines, stationList);

        return new Config(stationList, wagonList, locomotiveList, content);
    }

    private List<Config.Locomotive> readLocomotives(ArrayList<String> lines,
                                                    List<Config.Station> stationList) {

        LinkedList<Config.Locomotive> result = new LinkedList<>();
        Integer idGenerator = 0;

        for (String line : lines) {
            if (!line.startsWith("+locomotive")) {
                continue;
            }
            String[] params = line.split(" ");
            if (params.length != 3)
                throw new RuntimeException("Ошибка парсинга локомотива");

            String locomotiveName = params[1];
            String stationName = params[2];

            Config.Station initialStation = null;

            for (Config.Station station : stationList)
                if (Objects.equals(station.name, stationName))
                    initialStation = station;

            if (initialStation == null)
                throw new RuntimeException("Не найдена станция, которая была указана в качестве начальной для локомотива.");

            Config.Locomotive locomotive = new Config.Locomotive(idGenerator, locomotiveName, initialStation);
            result.add(locomotive);
        }

        return result;
    }


    private List<Config.Station> readStations(List<String> lines) {
        List<Config.Station> list = new LinkedList<>();
        for (String line : lines) {
            Config.Station station = readStationNameOnly(line);
            if (station != null) {
                list.add(station);
            }
        }

        Integer idGenerator = 0;

        for (String line : lines) {
            if (!line.startsWith("+station")) {
                continue;
            }
            String[] params = line.split(" ");
            if (params.length < 2) {
                continue;
            }

            for (Config.Station station : list) {
                if (Objects.equals(station.name, params[1])) {
                    station.id = idGenerator;
                    idGenerator++;

                    if (params.length != 3) {
                        continue;
                    }

                    // Parse roads
                    String roadsString = params[2];
                    for (String roadItem : roadsString.split(";")) {
                        String[] roadParams = roadItem.split("-");

                        String stationName = roadParams[0];
                        Long timeCost = Long.parseLong(roadParams[1]);

                        for (Config.Station roadStationSearch: list) {
                            if (roadStationSearch.name.equals(stationName)) {
                                station.roads.add(new Config.Station.Road(roadStationSearch, timeCost));
                            }
                        }
                    }
                }
            }
        }

        return list;
    }
    private Config.Station readStationNameOnly(String line) {
        if (!line.startsWith("+station")) {
            return null;
        }

        String[] params = line.split(" ");
        if (params.length < 2) {
            return null;
        }

        return new Config.Station(new LinkedList<>(), null, params[1]);
    }

    private List<Config.Wagon> readWagons(List<String> lines, List<Config.Station> stationList) {
        List<Config.Wagon> list = new LinkedList<>();
        Integer idGenerator = 0;
        for (String line : lines) {
            Config.Wagon wagon = readWagon(line, stationList);
            if (wagon != null) {
                list.add(wagon);
                wagon.id = idGenerator;
                idGenerator++;
            }
        }

        return list;
    }
    private Config.Wagon readWagon(String line, List<Config.Station> stationList) throws RuntimeException {
        if (!line.startsWith("+wagon")) {
            return null;
        }

        String[] params = line.split(" ");
        if (params.length != 4) {
            return null;
        }

        String wagonName = params[1];
        String initialStationName = params[2];
        String destinationName = params[3];

        Config.Station initial = null;
        Config.Station destination = null;

        for (Config.Station station : stationList) {
            if (station.name.equals(destinationName)) {
                destination = station;
            }
            if (station.name.equals(initialStationName)) {
                initial = station;
            }
        }

        if (initial == null || destination == null)
            throw new RuntimeException("Не указаны начальная и конечная станции");

        Config.Wagon wagon = new Config.Wagon(destination, null, wagonName);
        initial.wagons.add(wagon);

        return wagon;
    }

    private String readFileContents() {
        File file = getConfigFile();
        if (file == null) {
            return null;
        }

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            StringBuilder stringBuilder = new StringBuilder();
            String line = reader.readLine();

            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line = reader.readLine();
            }

            return stringBuilder.toString();
        } catch (IOException e) {
            return null;
        }
    }

    private File getConfigFile() {
        String currentPath = System.getProperty("user.dir");
        File currentDirectory = new File(currentPath);
        System.out.println(currentDirectory.getAbsolutePath());
        File[] childFiles = currentDirectory.listFiles();
        assert childFiles != null;
        for (File file : childFiles) {
            if (file.getName().equals(FILE_NAME)) {
                return file;
            }
        }

        return null;
    }

}
