package com.va.config;

import com.sun.istack.internal.Nullable;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class Config {

    public static class Station implements Serializable {

        public static class Road implements Serializable {
            public Station destination;
            public Long timeCost;

            Road(Station destination, Long timeCost) {
                this.destination = destination;
                this.timeCost = timeCost;
            }

            @Override
            public String toString() {
                return "DST=" + destination + " COST=" + timeCost;
            }
        }


        public List<Road> roads = new LinkedList<>();

        Integer id;

        public String name;

        public List<Wagon> wagons = new LinkedList<>();

        Station(List<Road> roads, Integer id, String name) {
            this.roads = roads;
            this.id = id;
            this.name = name;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Station && ((Station) obj).id.equals(id);
        }

        @Override
        public String toString() {
            return "NAME=" + name + " WAGONS_COUNT=" + wagons.size();
        }
    }

    public static class Wagon implements Serializable {
        public Station destination;
        Integer id;
        public String name;

        Wagon(Station destination, Integer id, String name) {
            this.destination = destination;
            this.id = id;
            this.name = name;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Wagon && ((Wagon) obj).id.equals(id);
        }

        @Override
        public String toString() {
            return " NAME=" + name + " DST=" + destination.name;
        }
    }

    public static class Locomotive implements Serializable {
        Integer id;
        public List<Wagon> wagons = new LinkedList<>();
        public Station currentStation;
        public String name;
        @Nullable
        public Station destination;


        @Override
        public boolean equals(Object obj) {
            return obj instanceof Locomotive && ((Locomotive) obj).id.equals(id);
        }

        Locomotive(Integer id, String name, Station currentStation) {
            this.name = name;
            this.currentStation = currentStation;
            this.id = id;
        }


    }

    public List<Station> stations;
    public List<Wagon> wagons;
    public List<Locomotive> locomotives;
    public String rawRepresentation;

    Config(List<Station> stations, List<Wagon> wagons, List<Locomotive> locomotives, String rawRepresentation) {
        this.stations = stations;
        this.wagons = wagons;
        this.locomotives = locomotives;
        this.rawRepresentation = rawRepresentation;
    }


    /*
       UTILS
     */

    public static String pathStringOf(List<Station> stations) {
        StringBuilder result = new StringBuilder();
        for (Config.Station station : stations) {
            result.append(" -> ").append(station.name);
        }

        return result.toString();
    }

}
