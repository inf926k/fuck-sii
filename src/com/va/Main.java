package com.va;

import com.va.agents.LocomotiveAgent;
import com.va.agents.StationAgent;
import com.va.agents.WagonAgent;
import com.va.config.Config;
import com.va.config.Config.Locomotive;
import com.va.config.Config.Station;
import com.va.config.Config.Wagon;
import com.va.config.ConfigParser;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;

/*
   Создание контейнера
   https://stackoverflow.com/questions/22391640/how-to-create-containers-and-add-agents-into-it-in-jade

 */
public class Main {

    private static ConfigParser configParser = new ConfigParser();

    public static Config config;

    public static void main(String[] args) throws Exception {
        config = configParser.parse();
        System.out.println("Configuration:");
        System.out.println(config.rawRepresentation);

        Runtime runtime = Runtime.instance();
        ProfileImpl profile = new ProfileImpl("localhost", 8888, null);
        ContainerController controller = runtime.createMainContainer(profile);


        System.out.println("Главный контейнер инициализирован.");
        System.out.println("Создание агентов...");
        System.out.println("------------------------------------------------");

        createAgents(config, controller);
    }

    private static void createAgents(Config config, ContainerController controller) throws StaleProxyException {
        // Stations
        for (Station station : config.stations) {
            controller.createNewAgent(station.name,
                    StationAgent.class.getName(),
                    new Station[]{station}).start();
        }

        // Wagons
        for (Wagon wagon : config.wagons) {
            controller.createNewAgent(wagon.name,
                    WagonAgent.class.getName(),
                    new Wagon[]{wagon}).start();
        }

        // Locomotives
        for (Locomotive locomotive : config.locomotives) {
            controller.createNewAgent(locomotive.name,
                    LocomotiveAgent.class.getName(),
                    new Locomotive[]{locomotive}).start();
        }
    }

}