package com.va;

import com.va.agents.LocomotiveAgent;
import com.va.agents.StationAgent;
import com.va.agents.WagonAgent;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    public static Logger shared = new Logger();


    public void log(String text) {
        System.out.println("[" + getTimePrefixString()+  "] " + text);
    }

    private String getTimePrefixString() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SSS");
        return format.format(new Date());
    }

    public void logCreation(WagonAgent wagonAgent) {
        log("Создан вагон с названием: " + wagonAgent.config.name);
    }

    public void logCreation(LocomotiveAgent locomotiveAgent) {
        log("Создан локомотив с названием: " + locomotiveAgent.config.name);
    }

    public void logCreation(StationAgent agent) {
        log("Создана станция с названием: " + agent.config.name);
    }
}
